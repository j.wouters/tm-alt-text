# tm-alt-text

Various tools to facilitate the creation of accessible document in GNU TeXmacs. Currently under development.

## Installation
To install this plugin, put the files in this repository in a folder `alt-text` under `$TEXMACS_HOME_PATH/plugins/` (i.e. under `~/.TeXmacs/plugins/alt-text` on Linux or Mac or `%appdata%\TeXmacs\plugins\alt-text` on Windows).

After installation, the directory structure should be as follows:
```
TEXMACS_HOME_PATH
└─── plugins
     └─── alt-text
          └─── progs
          └─── packages
```


## Use

After successful installation two new items "Link image with alt text" and "Insert image with alt text" will appear in the menus for image insertion. These images will have an alternative text associated with them upon conversion to html.
