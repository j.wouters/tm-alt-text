;; TODO: When the cursor is inside a alt-text tag, add a dialog to the toolbar to edit the alt text.

;; Dialog to enter alt text
(tm-widget (alt-text-form cmd)
  (form "Alt text"
    (centered
      (aligned
        (item (text "Alt text:")
          (form-input "Alt text" "string" '("") "1w"))))
    (bottom-buttons
      ("Cancel" (cmd #f)) >>
      ("Ok"
       (cmd (car (form-values)))))))

(tm-define (make-alt-link-image)
  (dialogue-window alt-text-form
         (lambda (x) (if x (begin
                      (insert-go-to `(html-attr "alt" ,x "") '(2 0))
                      (choose-file make-link-image "Load image" "image"))
                         ))
         "Alt text form"))

(tm-define (make-alt-inline-image)
  (dialogue-window alt-text-form
         (lambda (x) (if x (begin
                      (insert-go-to `(html-attr "alt" ,x "") '(2 0))
                      (choose-file make-inline-image "Load image" "image") )
                         ))
         "Alt text form"))

;;(tm-define (in-alt-text?)
;;    (inside? 'alt-text))

;;(tm-define (html-attr-context? t)
;;   (and t (tm-func? t 'html-attr)))

;;(menu-bind alt-text-functions-menu
;;       (-> "Edit alt text")
;;    )

;; TODO: create a menu for editing alt-text
;;(tm-menu (focus-alt-text-icon t)
;;   (:require (in-alt-text?))
;;   (:require (html-attr-context? t))
;    (=> (balloon (icon "tm_focus_save.xpm") "My menu")
;         (link embedded-save-menu)))


;; This only works for the document first loaded. Instead add it when inserting
;; a <alt-text> tag with a function make-alt-text like make-session
;;(if (url-exists? (url-unix "$TEXMACS_STYLE_PATH"
;;             "alt-text.ts"))
;;        (add-style-package "alt-text"))

;;(menu-bind plugin-menu
;;      (=> "Alt-text" (link alt-text-functions-menu)))

;; Insert items into image menu
(delayed (lazy-initialize-force) (menu-bind insert-image-menu
  (former)
  ---
    ("Link image with alt text" (make-alt-link-image))
    ("Insert image with alt text" (make-alt-inline-image))
  ))

;;(define-summarize summarized-alternative detailed-alternative)

(plugin-configure alt-text)
