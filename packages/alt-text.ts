<TeXmacs|2.1>

<style|source>

<\body>
  <inactive*|<assign|alt-text|<macro|main-obj|alt-text|<arg|main-obj>>>>

  <inactive*|<assign|tmhtml-alt-text|<macro|main-obj|alt-text|<compound|html-attr|alt|<arg|alt-text>|<arg|main-obj>>>>>

  \;

  <inactive*|<assign|summarized-alternative|<macro|x|y|<action|<html-attr|alt|<arg|y>|<arg|x>>|mouse-unfold|<arg|x>>>>>

  <inactive*|<assign|detailed-alternative|<macro|x|y|<action|alternative:
  <arg|y>|mouse-fold|alternative: <arg|y>>>>>
</body>

<initial|<\collection>
</collection>>